/***************************(C) COPYRIGHT 2014 root****************************/
// File Name     : app_main.c
// Author        : wlc
// Version       : V0.0.1
// Date          : 2014-1-1
// Description   : Main
// Notes         : Most of the register and device macros used here are defined in
//                 the file "msp430g2221.h"
/******************************************************************************/

    /* Hardware Mapping is:
    P1.0 = N_RST_LT8900
    P1.1 = N_SS_LT8900
    P1.2 = PKT_LT8900
    P1.3 = IO line to temp sensor
    P1.4 = D3V (enable power to LT and Sensor)
    P1.5 = SPI Clock
    P1.6 = MOSI
    P1.7 = MISO
    P2.6 = FIF0_LT8900
    P2.7 = BR_CLK_LT8900
    */

#include "msp430.h"
#include "lt89x0.h"
#include "ds18b20.h"


#define TX_PLOAD_WIDTH      13

typedef struct 
{
    unsigned char dev_id[8]; //64-bits of Device ID unique to each temperature sensor
    int data;
    int datarev;
    unsigned char data_count;
}tx_data_t;

tx_data_t g_tx_buf;

int temperature;


void main(void)
{
    volatile unsigned int i;

    WDTCTL = WDTPW + WDTHOLD;              // Stop watchdog timer
  
    /*Config IO*/
    P1DIR  =  (BIT0+BIT1+BIT3+BIT4); // P1 Pins 0,1,3,4 are OPs
    P1OUT &=~BIT4; // Turn ON power to sensor and RF chip
    
    // COnfigure SPI for LT8900
    USICTL0 |= USIPE7 +  USIPE6 + USIPE5 + USIMST + USIOE; // Port, SPI master
    USICKCTL = USIDIV_4 + USISSEL_2;       // /16 SMCLK
    USICTL0 &= ~USISWRST;                  // USI released for operation
    
    // Get the temperature sensor's ID
    ds18b20_read_id(&g_tx_buf.dev_id[0]); 
    
    // To select a precise frequency a better way is to use the calibrated settings:
    // See -> http://www.argenox.com/library/msp430/ch6-clocks.php
    BCSCTL1|=0x30; // RSelx = 0 and ACLK Divider = /4
    BCSCTL3|=0x20; // LowFrequencyClock = VLOCLK
    
    // Timer A is used to time the transmission of temperature
    // The MCU wakes up every (4000 + 4000)*6 counts, sends the data and goes back to sleep  
    CCTL0 = CCIE;                          // CCR0 interrupt enabled
    CCR0 = 4000;                                
    TACTL = TASSEL_1 + MC_3;               // SMCLK, contmode (Govind: ACLK not SMCLK? and Count Mode = UP/DOWN)
    
    while(1)
    {
      P1OUT &=~BIT4; // Power ON
      ds18b20_start(); // Start an ADC conversion
      lt89x0_init(); // Init the wireless chip
      g_tx_buf.data=ds18b20_read(); // Returned value is Tc * 100
      g_tx_buf.datarev=g_tx_buf.data; // Copy the same thing again? Why?
      g_tx_buf.data_count++; // Count of conversions done
      lt89x0_send_data((unsigned char *)&g_tx_buf,13);
      spiWriteReg(35, 0xC3, 0x00); // Power Down + Sleep + Scramble data = 11b. Expected current = 1uA
      P1OUT |=BIT4; // Power Off the sensor and Wireless chip
      _BIS_SR(LPM3_bits + GIE);             // Enter LPM3 w/ interrupt 
    }
    
} 

#pragma vector=TIMERA0_VECTOR
//#pragma vector=TIMER0_A0_VECTOR
// If you don't specificaly exit the Low power mode in the ISR, 
// execution will go back to the original state
__interrupt void Timer_A (void)
{    
    static int i=0;
    
    if(++i>6)
    {
      i=0;
      __bic_SR_register_on_exit(LPM3_bits); // Exit the Low Power Mode
    }
}