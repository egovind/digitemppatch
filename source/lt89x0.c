/***************************(C) COPYRIGHT 2014 root****************************/
// File Name     : lt89x0.c
// Author        : Root W.L.C.
// Version       : V0.0.1
// Date          : 2014-4-14
// Description   : Main
/******************************************************************************/
#include "lt89x0.h"
#include "msp430.h"

unsigned char RegH, RegL;

//-----------------------------------------------------------------------------
unsigned char spiReadWrite(unsigned char Byte)
{ 
    USISRL = Byte;                        // init-load data
    USICNT = 8;                           // init-load counter
    while (!(USIIFG & USICTL1));          // Counter clear?
    Byte  = USISRL;
    return (Byte);
}

//----------------------------------------------------------------------------
void spiReadreg(unsigned char reg)
{
    SSL(); 
    spiReadWrite(READ | reg);
    RegH = spiReadWrite(0x00);
    RegL = spiReadWrite(0x00);
    SSH(); 
}

//----------------------------------------------------------------------------
void spiWriteReg(unsigned char reg, unsigned char byteH, unsigned char byteL)
{
    SSL();  
    spiReadWrite(WRITE & reg);
    spiReadWrite(byteH);
    spiReadWrite(byteL);
    SSH(); 
}

//----------------------------------------------------------------------------
void lt89x0_init(void)
{
  // Delays can probably be reduced ?
    RSTL();
    delay_ms(100);
    RSTH();
    delay_ms(200);
  
   // See "recommended Register values" in datasheet.
    spiWriteReg(0, 0x6f, 0xe0); // 0x6fEf ?
    spiWriteReg(1, 0x56, 0x81);
    spiWriteReg(2, 0x66, 0x17);
    spiWriteReg(4, 0x9c, 0xc9);
    spiWriteReg(5, 0x66, 0x37);
    spiWriteReg(7, 0x00, 0x00); // No Tx/Rx for now, OTA frequency = 2402
    spiWriteReg(8, 0x6c, 0x90);
    spiWriteReg(9, 0x48, 0x00); // PA current = 2, Gain = 8				
    spiWriteReg(10, 0x7f, 0xfd);
    spiWriteReg(11, 0x00, 0x08);
    spiWriteReg(12, 0x00, 0x00);
    spiWriteReg(13, 0x48, 0xbd);
    
    spiWriteReg(22, 0x00, 0xff);
    spiWriteReg(23, 0x80, 0x05); // Calibrate VCO before each Tx/Rx
    spiWriteReg(24, 0x00, 0x67);
    spiWriteReg(25, 0x16, 0x59);
    spiWriteReg(26, 0x19, 0xe0);
    spiWriteReg(27, 0x13, 0x00); // 1200?
    spiWriteReg(28, 0x18, 0x00);
    
    spiWriteReg(32, 0x50, 0x00);
 //   spiReadreg(32);
    spiWriteReg(33, 0x3f, 0xc7); 
    spiWriteReg(34, 0x20, 0x00); 
    spiWriteReg(35, 0x03, 0x00);
    spiWriteReg(36, 0x03, 0x80); // Node Address
    spiWriteReg(37, 0x03, 0x80); // Node Address
    spiWriteReg(38, 0x5a, 0x5a); // Node Address
    spiWriteReg(39, 0x03, 0x80); // Node Address
    spiWriteReg(40, 0x44, 0x01);
    spiWriteReg(41, 0xB0, 0x00);  // crc ON, scramble = off, 1st byte of payload = packet length
    spiWriteReg(42, 0xfd, 0xb0);  
    spiWriteReg(43, 0x00, 0x0f);
    spiWriteReg(50, 0x00, 0x00); // Clear FIFO
    delay_ms(200);
	
    spiWriteReg(7, 0x01, 0x00);
    delay_ms(2);
    spiWriteReg(7, 0x00, 0x30); // OTA channel = 0x30 + 2402
}
// Routine to send a packet. Note that first byte of packet is the length
// So max packet length is 255
void lt89x0_send_data(unsigned char *send_data,unsigned char len)
{
    unsigned char i=0;
    unsigned regh,regl;
    unsigned int cnt=0;
    
    
    /*清空发送缓存区*/
    spiWriteReg(7, 0x00, 0x00); // Clear Tx/Rx, OTA channel
    spiWriteReg(52, 0x80, 0x00); // Clear Tx FIFO pointer
    
    spiWriteReg(50, len, *send_data++); // Fill the FIFO with the Length byte and the first payload byte
    
    // Copy the rest of the payload to the FIFO
    for(i=0;i<(len/2);i++)
    {
      regh=*send_data++;
      regl=*send_data++;
      spiWriteReg(50,regh,regl);  
    }
    
    /*允许发射使能*/
    spiWriteReg(7, 0x01, 0x30);	 // Enable the transmitter and select channel 0x30 
// PKT flag goes HIGH after a successful transmission, so wait for that.					
    while ((P1IN & BIT2)==0)
    {
      if(++cnt>30000){ // A timeout
        break;
      }
    }
    spiWriteReg(7, 0x00, 0x30); // Stop the transmitter
//   delay_ms(1);
    spiWriteReg(52, 0x00, 0x80);   // ? whats happening here? 
    spiWriteReg(7, 0x00, 0xB0);	// Enable receiver
}

//// Port 1 interrupt service routine
//#pragma vector=PORT1_VECTOR
//__interrupt void Port_1(void)
//{
//    P1IE  &=  ~BIT2;                        // interrupt enabled
//    spiWriteReg(7, 0x00, 0x30);
//    delay_ms(3);
//    spiWriteReg(52, 0x00, 0x80);    // 清接收缓存区
//    spiWriteReg(7, 0x00, 0xB0);	    // 允许接收使能
//    delay_ms(50);
//    P1IFG &= ~BIT2; 
//    P1IE  |=  BIT2;                        // interrupt enabled
//}
