/***************************(C) COPYRIGHT 2014 root****************************/
// File Name     : lt89x0.h
// Author        : Root W.L.C.
// Version       : V0.0.1
// Date          : 2014-4-14
// Description   : include for lt89x0
/******************************************************************************/
#ifndef _LT89X0_H
#define _LT89X0_H

#define delay_ms(x) __delay_cycles((double)x*12000/10) 

#define WRITE		0x7F
#define READ		0x80

#define RSTL()                  P1OUT &=~BIT0
#define RSTH()                  P1OUT |=BIT0

#define SSL()                   P1OUT &=~BIT1
#define SSH()                   P1OUT |=BIT1

void lt89x0_init(void);
void lt89x0_send_data(unsigned char *send_data,unsigned char len);
void spiWriteReg(unsigned char reg, unsigned char byteH, unsigned char byteL);
#endif
