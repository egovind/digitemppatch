
#ifndef __DS18B20_H
#define __DS18B20_H
   
#define delay_us(x) __delay_cycles((long)x*12/10) 


#define  SkipROM    	0xCC  //Address all connected devices on the bus
#define  SearchROM  	0xF0  //搜索ROM
#define  ReadROM    	0x33  //读ROM
#define  MatchROM   	0x55  //匹配ROM
#define  AlarmROM   	0xEC  //告警ROM
#define  StartConvert    	0x44  //开始温度转换，在温度转换期间总线上输出0，转换结束后输出1
#define  ReadScratchpad  	0xBE  //Read the whole scratchpad from 0 - 8
#define  WriteScratchpad 	0x4E  //Write Scratchpad bytes 2 - 4
#define  CopyScratchpad  	0x48  //Copy bytes 2 - 4 to EEPROM
#define  RecallEEPROM    	0xB8  //Force load 2 - 4 from EE to RAM
#define  ReadPower       	0xB4  //读电源的供电方式：0为寄生电源供电；1为外部电源供电

/* DS Scratchpad looks like this:
0 - Temp LSB (0x50)
1 - Temp MSB (0x05)
2 - Alarm Trigger High or User Byte 1 (EE Backup)
3 - Alarm Trigger Low or User byte 2 (EE Backup)
4 - Configuration register (EE Backup)
5 - Reserved 0xFF
6 - Reserved
7 - Reserved 0x10
8 - CRC for 0 - 7 of scratchpad
*/

void ds18b20_start(void);
int ds18b20_read(void);	
void ds18b20_read_id(unsigned char *ds18b20_id);

#endif
