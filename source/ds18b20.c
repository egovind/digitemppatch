/**
 ******************************************************************
 *				(c) Copyright 2012 Root Studio
 *                    All Rights Reserved
 * file    ds18b20.c
 * brief  
 * author  Root W.L.C.
 * date    04/06/2014
 * version 0.1a create orignal file
 *******************************************************************
 */
#include "msp430.h"  
#include "DS18B20.h"  


#define DS_PORT   		GPIOC  
#define DS_DQIO  		GPIO_Pin_13  
#define DS_RCC_PORT  		RCC_APB2Periph_GPIOC  
#define DS_PRECISION 		0x7f  // 12-bit resolution, max conversion time is 720mS  	  
#define DS_AlarmTH  		0x64  
#define DS_AlarmTL  		0x8a  
#define DS_CONVERT_TICK 	                1000 

 

#define DS_REG_NUM	                9 
// For Reset sequence details see Initialization sequence in datasheet
// All communication to DS must begin with a Reset sequence   
void ResetDS18B20(void)  
{      
    P1DIR  |=  BIT3;
    P1OUT  |=  BIT3;  
    delay_us(50);  
	
    P1OUT  &= ~BIT3;  

    delay_us(400);  //500us （该时间的时间范围可以从480到960微秒）  
    P1OUT  |=  BIT3;   
    delay_us(40);  //40us  
    
    P1DIR  &= ~BIT3;
    while(!(BIT3 & P1IN));  
    delay_us(500);  //500us  
    P1DIR  |=  BIT3;
    P1OUT  |=  BIT3;  
}  
  
void DS18B20WriteByte(unsigned char Dat)  
{  
    unsigned char i;  
    for(i=8;i>0;i--)  
    {  
      P1DIR  |=  BIT3;     //在15u内送数到数据线上，DS18B20在15-60u读数
      P1OUT  &= ~BIT3;
      delay_us(5);    //5us  
      if(Dat & 0x01){  
        P1OUT  |=  BIT3;
      } 
      else 
      { 
        P1OUT  &=  ~BIT3;
      }
      delay_us(65);    //65us  
      P1OUT  |=  BIT3;  
      delay_us(2);    //连续两位间应大于1us  
      Dat >>= 1;   
    }   
}  
  
  
unsigned char DS18B20ReadByte(void)  
{  
    unsigned char i,Dat;  
    P1DIR  |=  BIT3;  
    P1OUT  |=  BIT3;
    delay_us(5);  
    for(i=8;i>0;i--)  
    {  
      Dat >>= 1;  
      P1OUT  &=  ~BIT3;     //从读时序开始到采样信号线必须在15u内，且采样尽量安排在15u的最后  
      delay_us(5);   //5us  
      P1OUT  |=  BIT3;  
      P1DIR  &= ~BIT3;
      delay_us(5);   //5us
      
      if(BIT3 & P1IN)
      {
        Dat|=0x80;
      }
      else
      { 
        Dat&=0x7f;
      }
      delay_us(65);   //65us  
      P1DIR  |=  BIT3;  
      P1OUT  |=  BIT3;  
    }  
    return Dat;  
}  
  
  
void ReadRom(unsigned char *Read_Addr)  
{  
    unsigned char i;  
                    
    DS18B20WriteByte(ReadROM);  
                    
    for(i=8;i>0;i--)  
    {  
      *Read_Addr=DS18B20ReadByte();  
      Read_Addr++;  
    }  
}  
  
  
void DS18B20Init(unsigned char Precision,unsigned char AlarmTH,unsigned char AlarmTL)  
{    
    __disable_interrupt();  
    ResetDS18B20();  
    DS18B20WriteByte(SkipROM);   
    DS18B20WriteByte(WriteScratchpad);  
    DS18B20WriteByte(AlarmTL);  
    DS18B20WriteByte(AlarmTH);  
    DS18B20WriteByte(Precision);  
                    
    ResetDS18B20();  
    DS18B20WriteByte(SkipROM);   
    DS18B20WriteByte(CopyScratchpad);  
    __enable_interrupt(); 
    
    P1DIR  &= ~BIT3;                
    while(!(BIT3 & P1IN)); 
}  
  
  
void DS18B20StartConvert(void)  
{   
    __disable_interrupt(); 
    ResetDS18B20();  
    DS18B20WriteByte(SkipROM);   
    DS18B20WriteByte(StartConvert);   
    __enable_interrupt(); 
}  
 
  
void ds18b20_start(void)  
{  
    DS18B20Init(DS_PRECISION, DS_AlarmTH, DS_AlarmTL);  
    DS18B20StartConvert();  
}  

unsigned char CalculateCRCValue(unsigned char ucNewData,unsigned char ucCRCValue)
{
    unsigned char bXorValue,ucTempValue,ucCount;

    for(ucCount=0; ucCount<8; ucCount++)
    {
       
        bXorValue = (ucNewData ^ ucCRCValue)& 0x01;
        
        if(bXorValue)
        {
            ucTempValue = ucCRCValue ^ 0x18;
        }
        else
        {
            ucTempValue = ucCRCValue;
        }
        ucTempValue >>= 1;

        if(bXorValue)
        {
            ucCRCValue = ucTempValue | 0x80;
        }
        else
        {
            ucCRCValue = ucTempValue;
        }
        
        ucNewData >>= 1;
    }

    return ucCRCValue;
}

// To verify the CRC on the sensor RAM
unsigned char DS18B20_CheckCRC8(unsigned char *ucDataBuff,unsigned char ucDataLength)
{
    unsigned char ucCRCValue, ucCount;

    ucCRCValue = 0;

    for(ucCount=0; ucCount<ucDataLength; ucCount++)
    {
        ucCRCValue = CalculateCRCValue(ucDataBuff[ucCount],ucCRCValue);
    }

    if(ucCRCValue == 0)
    {
        return 1;
      }
    else
    {
        return 0;
      }
}

// Each SD18B20 sensor has a unique 64-bit ROM ID stored internally.
// This function reads out the ID
void ds18b20_read_id(unsigned char *ds18b20_id)  
{  
    unsigned char i;
    
    __disable_interrupt();  
    ResetDS18B20();  
    DS18B20WriteByte(ReadROM);   
    for(i=0;i<8;i++)
    {
      ds18b20_id[i]=DS18B20ReadByte();
    }   
    ResetDS18B20();  
    __enable_interrupt();   
} 

// This routine reads the entire scratchpad of the sensor (9 bytes)
// If we only need the temperature, it's enough to read the first 2 bytes and issue a reset.
int ds18b20_read(void)  
{  
    unsigned char t_data[DS_REG_NUM],i;
    long m; 
    long n; 
   
    __disable_interrupt();  
    ResetDS18B20();  
    DS18B20WriteByte(SkipROM);   
    DS18B20WriteByte(ReadScratchpad); 
    for(i=0;i<DS_REG_NUM;i++)
    {
      t_data[i]=DS18B20ReadByte();
    }   
    ResetDS18B20();  
    __enable_interrupt();   
    
    DS18B20_CheckCRC8(t_data,DS_REG_NUM); // Verify checksum. FIXME: Nothing done if Checksum is Wrong
    
    // At 12 bit resolution 1 count = 0.0625 degree celsius
    m=((t_data[1]<<8) | t_data[0]);
    n= m*625/10000;  // Temp in Celsius
    m=n*100; // Tc * 100
    // t_data[7] is always 0x10, while t_data[6] is reserved so what's happening here?
    m=(m-25)+(t_data[7]-t_data[6])*100/t_data[7];
    
    DS18B20StartConvert();  // Start another conversion. Why? FIXME:
                    
    return  m;  
}  


